<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 15.07.18
 * Time: 20:47
 */

namespace App\Interfaces;


interface CommentInterface
{
    public function setText(string $text);
    public function getText(): string ;
    public function getAuthor(): UserInterface;
    public function getAdvert(): AdvertInterface;
}