<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Comment;
use App\Http\Requests\StoreCommentRequest;
use App\Http\Requests\UpdateCommentRequest;
use App\Transformers\CommentTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function store(StoreCommentRequest $request, Advert $advert) {
        $comment = new Comment();
        $comment->setText($request->text);
        $comment->user()->associate($request->user());
        $comment->advert()->associate($advert);

        $comment->save();

        return fractal()
            ->item($comment)
            ->transformWith(new CommentTransformer())
            ->toArray();
    }

    public function update(UpdateCommentRequest $request, Advert $advert, Comment $comment){
        try {
            $this->authorize('update', $comment);
        } catch (AuthorizationException $e) {
        }

        $comment->setText($request->get('text', $comment->getText()));

        $comment->save();

        return fractal()
            ->item($comment)
            ->transformWith(new CommentTransformer())
            ->toArray();
    }

    public function destroy(Advert $advert, Comment $comment){
        try {
            $this->authorize('destroy', $comment);
            $comment->delete();
        } catch (\Exception $e) {
        }
    }
}
