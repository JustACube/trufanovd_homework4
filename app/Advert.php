<?php

namespace App;

use App\Interfaces\AdvertInterface;
use App\Interfaces\UserInterface;
use Illuminate\Database\Eloquent\Model;

class Advert extends Model implements AdvertInterface
{

    protected $fillable = [
        'tittle', 'description',  'image', 'price', 'city'
    ];


    public function user(){
        return $this->belongsTo(User::class);
    }
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function favorites(){
        return $this->belongsToMany(User::class,'favorites')->withTimestamps();
    }
    public function metrics(){
        return $this->hasMany(Metric::class);
    }
    public function subcategory(){
        return $this->belongsTo(SubCategory::class);
    }




    //
    public function getInfo(): array
    {
        // TODO: Implement getInfo() method.
    }

    public function getMetrics()
    {
        return $this->metrics()->get();
    }

    public function getComments()
    {
        return $this->comments()->get();
    }

    public function getRootCategory()
    {
        // TODO: Implement getRootCategory() method.
    }

    public function getCategory()
    {
        return $this->subcategory()->first();
    }

    public function getCountInFavorite(): int
    {
        // TODO: Implement getCountInFavorite() method.
    }

    public function getAuthor(): UserInterface
    {
        return $this->user()->first();
    }

    public function findByStr(string $phrase): AdvertInterface
    {
        // TODO: Implement findByStr() method.
    }

    public function setTittle(string $tittle)
    {
        $this->tittle=$tittle;
    }

    public function getTittle(): string
    {
        return $this->tittle;
    }

    public function setDescription(string $description)
    {
        $this->description=$description;
    }

    public function getDescription(): string
    {
       return $this->description;
    }

    public function setImage(string $href)
    {
        $this->image=$href;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setPrice(int $price)
    {
        $this->price=$price;
    }

    public function getPrice(): int
    {
        return $this->price;
    }
}
