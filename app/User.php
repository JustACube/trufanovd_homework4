<?php

namespace App;

use App\Interfaces\UserInterface;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable implements UserInterface
{
    use Notifiable, HasApiTokens;


    protected $fillable = [
        'firstName', 'lastName',  'email', 'password', 'city'
    ];


    protected $hidden = [
        'password', 'remember_token',
    ];

    /*
     * Relationships
     */

    public function adverts(){
        return $this->hasMany(Advert::class);
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function favorites(){
        return $this->belongsToMany(Advert::class,'favorites')->withTimestamps();
    }

    /*
     * Getters+setters
     */

    public function getInfo(): array
    {
        // TODO: Implement getInfo() method.
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }

    public function setFirstName(string $fName)
    {
        $this->firstName = $fName;
    }

    public function getLastName(): string
    {
        return $this->lastName;
    }

    public function setLastName(string $lName)
    {
        $this->lastName = $lName;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    public function getCity(): string
    {
        return $this->city;
    }

    public function setCity(string $city)
    {
        $this->city = $city;
    }

    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    public function ownsAdvert(Advert $advert){
        return $this->id === $advert->user->id;
    }

    public function ownsComment(Comment $comment){
        return $this->id === $comment->user->id;
    }
}
