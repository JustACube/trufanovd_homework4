<?php

namespace App\Http\Controllers;

use App\Advert;
use App\Http\Requests\StoreAdvertRequest;
use App\Http\Requests\UpdateAdvertRequest;
use App\Transformers\AdvertTransformer;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class AdvertController extends Controller
{
    public function index(){
        $adverts = Advert::all();

        return fractal()
            ->collection($adverts)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function show(Advert $advert)
    {
        return fractal()
            ->item($advert)
            ->parseIncludes(['comments'])
            ->transformWith(new AdvertTransformer())
            ->toArray();

    }

    public function store(StoreAdvertRequest $request){
        $advert = new Advert();
        $advert->setTittle($request->tittle);
        $advert->setDescription($request->description);
        $advert->setImage($request->image);
        $advert->setPrice($request->price);
        $advert->status = true; //todo
        $advert->category_id = $request->categoryId; //todo

        $advert->user()->associate($request->user());

        $advert->save();

        return fractal()
            ->item($advert)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function update(UpdateAdvertRequest $request, Advert $advert){
        try {
            $this->authorize('update', $advert);
        } catch (AuthorizationException $e) {
        }

        $advert->setTittle($request->get('tittle', $advert->getTittle()));
        $advert->setDescription($request->get('description', $advert->getDescription()));
        $advert->setImage($request->get('image', $advert->getImage()));
        $advert->setPrice($request->get('price', $advert->getPrice()));

        $advert->save();

        return fractal()
            ->item($advert)
            ->transformWith(new AdvertTransformer())
            ->toArray();
    }

    public function destroy(Advert $advert){
        try {
            $this->authorize('destroy', $advert);
            $advert->delete();
        } catch (\Exception $e) {
        }
    }

    public function favorite(Request $request, Advert $advert){
        $advert->favorites()->attach($request->user());
    }
}
