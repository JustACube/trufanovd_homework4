<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {    // реализовать в модели
    return $request->user();                                                 // todo
});                                                                          //

Route::post('/register', 'RegisterController@register');

Route::group(['prefix' => 'adverts'], function (){
    Route::post('/', 'AdvertController@store')->middleware('auth:api');
    Route::get('/', 'AdvertController@index');
    Route::get('/{advert}', 'AdvertController@show');
    Route::put('/{advert}', 'AdvertController@update')->middleware('auth:api');
    Route::delete('/{advert}', 'AdvertController@destroy')->middleware('auth:api');

    Route::post('/{advert}', 'AdvertController@favorite')->middleware('auth:api'); // todo http метод

    Route::group(['prefix' => '/{advert}/comments'], function () {
        Route::post('/', 'CommentController@store')->middleware('auth:api');
        Route::put('/{comment}', 'CommentController@update')->middleware('auth:api'); // todo
        Route::delete('/{comment}', 'CommentController@destroy')->middleware('auth:api');
    });
});