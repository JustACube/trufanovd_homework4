<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 15.07.18
 * Time: 1:59
 */

namespace App\Interfaces;


interface UserInterface
{
    public function getInfo(): array;
    //public function getAdverts();
    //public function getFavorites();
    //public function changePassword;
    //public function logout();
    //public function updateProfile();

    public function getFirstName() : string ;
    public function setFirstName(string $fName);
    public function getLastName():string ;
    public function setLastName(string $lName);
    public function getEmail():string ;
    public function setEmail(string $email);
    public function getCity():string ;
    public function setCity(string $city);
    public function setPassword(string $password);
}