<?php

namespace App\Policies;

use App\Advert;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AdvertPolicy
{
    use HandlesAuthorization;

    public function update(User $user, Advert $advert){
        return $user->ownsAdvert($advert);
    }

    public function destroy(User $user, Advert $advert){
        return $user->ownsAdvert($advert);
    }
}
