<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    public function mainCategory(){
        return $this->belongsTo(MainCategory::class);
    }
    public function adverts(){
        return $this->hasMany(Advert::class);
    }
}
