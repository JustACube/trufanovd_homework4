<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 15.07.18
 * Time: 12:59
 */

namespace App\Transformers;


use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user){
        return [
            'username' => $user->getEmail() . ' successfully registered',
        ];
    }
}