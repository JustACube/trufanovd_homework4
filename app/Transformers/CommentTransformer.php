<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 15.07.18
 * Time: 21:30
 */

namespace App\Transformers;


use App\Comment;
use League\Fractal\TransformerAbstract;

class CommentTransformer extends TransformerAbstract
{
    public function transform(Comment $comment){
        return [
            'text'=> $comment->getText(),
            'createdAt' => $comment->created_at->toDateTimeString(),
            'byUser' => $comment->getAuthor()->getEmail()
        ];
    }
}