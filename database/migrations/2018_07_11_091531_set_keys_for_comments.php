<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetKeysForComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->unique(['user_id','advert_id']); // TODO

            $table->foreign('user_id')->references('id')->on('users'); //TODO
            $table->foreign('advert_id')->references('id')->on('adverts'); //TODO
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropUnique(['user_id','advert_id']);

            $table->dropForeign('user_id');
            $table->dropForeign('advert_id');
        });
    }
}
