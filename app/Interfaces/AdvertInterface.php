<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 15.07.18
 * Time: 10:56
 */

namespace App\Interfaces;


interface AdvertInterface
{
    public function getInfo():array ;
    public function getMetrics();
    public function getComments() ;
    public function getRootCategory(); //todo
    public function getCategory();
    public function getCountInFavorite(): int ;
    public function getAuthor(): UserInterface;
    public function findByStr(string $phrase):AdvertInterface;

    public function setTittle(string $tittle);
    public function getTittle(): string ;
    public function setDescription(string $description);
    public function getDescription(): string ;
    public function setImage(string $href);
    public function getImage(): string ;
    public function setPrice(int $price);
    public function getPrice(): int ;

}