<?php

namespace App;

use App\Interfaces\AdvertInterface;
use App\Interfaces\CommentInterface;
use App\Interfaces\UserInterface;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model implements CommentInterface
{

    protected $fillable = [
        'text'
    ];

    public function getText() : string
    {
        return $this->text;
    }
    public function setText(string $text){
        $this->text=$text;
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function advert(){
        return $this->belongsTo(Advert::class);
    }

    public function getAuthor(): UserInterface
    {
        return $this->user()->first();
    }

    public function getAdvert(): AdvertInterface
    {
        return $this->advert()->first();
    }
}
